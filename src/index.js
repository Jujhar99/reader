import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import Sidebar from './Sidebar';
import Read from './Read';

class Reader extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0, // 0 = home, 2 = article reader
      latestNews: 0,
      searchTerm: '',
      searchedNews: 0, // all related articles
      position: 0, // position of selected article
      activePage: 1,
      content: '', // article content
      url: '',
      paginationAmount: 4, // elements per page
    };

    this.returnHome = this.returnHome.bind(this);
    this.articleRead = this.articleRead.bind(this);
    this.newPage = this.newPage.bind(this);
    this.searchInputChange = this.searchInputChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);

    axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=')
      .then((res) => {
        this.setState({
          latestNews: res.data,
        });
        // console.log(this.state.latestNews.articles);
      });
  }

  /* pagination */
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }

  articleRead(content, url) {
    console.log('Article opened');

    let articlePosition = 0;

    // get article position
    this.state.latestNews.articles.forEach((val, index) => {
      // console.log(val.content + index);
      if (val.content === content) {
        articlePosition = index;
        // console.log('found');
      }
    });

    this.setState({
      content,
      url,
      position: articlePosition,
      page: 2,
    });
  }

  newPage() {
    console.log('Page change');

    axios.get(`https://newsapi.org/v2/everything?q=${this.state.searchTerm}&sortBy=popularity&apiKey=`)
      .then((res) => {
        if (res.data !== '') {
          // pagination
          const searchDat = res.data;
          // const high = ((this.state.activePage) * this.state.paginationAmount);
          // const low = high - 4;

          // searchDat = searchDat.slice(low, high);

          this.setState({
            searchedNews: searchDat,
            page: 0,
          });
        }
      });
  }

  returnHome() {
    console.log('You are returning home');
    this.setState({
      page: 0,
    });
  }

  searchInputChange(event) {
    console.log(`Search term changed${this.state.searchTerm}`);
    this.setState({ searchTerm: event.target.value });
  }

  render() {
    /* search list page */
    if (this.state.page === 1) {
      return (
        <div>
          <Sidebar action={this.returnHome} />
          <Read searchTerm={this.state.searchTerm} searchedNews={this.state.searchedNews} />
        </div>
      );
    }

    /* article page */
    if (this.state.page === 2) {
      // for previous and next articles
      let contentTree;
      if (this.state.searchTerm === '') {
        contentTree = this.state.latestNews;
      } else {
        contentTree = this.state.searchedNews;
      }

      return (
        <div>
          <Sidebar action={this.returnHome} />
          <Read
            searchTerm={this.state.searchTerm}
            searchedNews={this.state.searchedNews}
            position={this.state.position}
            contentTree={contentTree}
            content={this.state.content}
            url={this.state.url}
          />
        </div>
      );
    }

    let latestNewsOut = '';
    let resultsOutput = '';

    if (this.state.latestNews !== 0) {
      // Display search results if search submitted
      if (this.state.searchedNews !== 0) {
        resultsOutput = this.state.searchedNews;
      } else {
        resultsOutput = this.state.latestNews;
      }

      latestNewsOut = resultsOutput.articles.map(({
        title, description, content, url, urlToImage, publishedAt, author,
      }) => (
        <div
          onClick={() => (this.articleRead(content, url))}
          onKeyPress={() => (this.articleRead(content, url))}
          role="button"
          tabIndex={0}
        >
          <br />
          <img src={urlToImage} height="50px" alt={`${urlToImage}`} />
          <br />
          {title}
          {' '}
          {description}
          <span className="red-text">
          &nbsp;
            {author}
            {' '}
•

          &nbsp;
            { (new Date(publishedAt)).toUTCString().split(' ').slice(0, 5)
              .join(' ') }
          </span>
        </div>
      ));
    }

    // pagination
    const high = ((this.state.activePage) * this.state.paginationAmount);
    const low = high - 4;

    latestNewsOut = latestNewsOut.slice(low, high);

    /* home page */
    return (
      <div>
        <Sidebar action={this.returnHome} />
        <div id="home-left">
          <h2 className="gold-text">Jujhar S.</h2>
          <br />
          <div>Search News</div>
          <form onSubmit={e => (e.preventDefault())}>
            <input type="text" onChange={this.searchInputChange} value={this.state.searchTerm} />
            <button type="submit" onClick={this.newPage}>Submit</button>
          </form>
        </div>
        <div id="home-right">
          {latestNewsOut}

          <br />
          <Pagination
            hideDisabled
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.paginationAmount}
            totalItemsCount={20}
            onChange={this.handlePageChange}
          />

          <div
            onClick={this.newPage}
            onKeyPress={this.newPage}
            role="button"
            tabIndex={0}
          >
            <br />
            <br />
            {' '}
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Reader />, document.getElementById('react'));
