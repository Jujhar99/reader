import React from 'react';
import PropTypes from 'prop-types';

class Reader extends React.Component {
  constructor(props) {
    super(props);

    this.linka = this.linka.bind(this);
  }

  linka() {
    console.log('link fired');
    this.props.action();
  }

  render() {
    return (
      <div id="sidebar-wrapper">

        <div
          onClick={this.linka}
          onKeyPress={this.linka}
          role="button"
          tabIndex={0}
        >
          {' '}

          Home
        </div>
        <br />
        <img src="assets/settings.png" alt="settings" />
      </div>
    );
  }
}

Reader.propTypes = {
  action: PropTypes.func,
};

Reader.defaultProps = {
  action(e) { return e; },
};

export default Reader;
