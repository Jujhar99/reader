import React from 'react';
import PropTypes from 'prop-types';

let searchedNewsOut = 'loading..';


class Read extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      content: this.props.content,
      position: this.props.position,
      url: this.props.contentTree.articles[this.props.position].url,
    };

    this.jumpArticle = this.jumpArticle.bind(this);

    if (this.props.searchedNews !== 0) {
      searchedNewsOut = this.props.searchedNews.articles.map(({ title, description, url }) => (
        <div
          onClick={url}
          onKeyPress={url}
          role="button"
          tabIndex={0}
        >
          {title}
          {' '}
          {description}
          <br />
          <br />
        </div>
      ));
    }
  }

  jumpArticle(direction) {
    console.log(`Going to article change: ${direction}`);
    let newPos = 0;

    // get next article
    if (direction === 1) {
      newPos = this.state.position + 1;

      if (newPos === this.props.contentTree.articles.length) newPos = 0;
    }

    // get previous article
    if (direction === -1) {
      newPos = this.state.position - 1;

      if (newPos === 0) newPos = this.props.contentTree.articles.length;
    }

    this.setState({
      content: this.props.contentTree.articles[newPos].content,
      position: newPos,
      url: this.props.contentTree.articles[newPos].url,
    });
  }

  render() {
    if (this.state.content !== '') {
      return (
        <div id="reader-wrapper">
          <div id="reader-page">
            {(this.props.searchTerm) ? (`${this.props.searchTerm} news`)
              : 'Latest news'}
            <br />
            <br />
            <a href={this.state.url} target="_blank" rel="noopener noreferrer">

            Read full
            </a>
            <br />
            <span className="article-text">
              {(this.state.content !== null)
                ? this.state.content : 'No description linked to article please click link above.'}
            </span>
            <br />
            <br />


            <span
              onClick={() => (this.jumpArticle(-1))}
              onKeyPress={() => (this.jumpArticle(-1))}
              role="button"
              tabIndex={0}
            >
Past article
            </span>
            {' '}
            <span
              onClick={() => (this.jumpArticle(1))}
              onKeyPress={() => (this.jumpArticle(1))}
              role="button"
              tabIndex={0}
            >
| Next article
            </span>
          </div>
        </div>
      );
    }

    /* Article read */

    return (
      <div id="reader-wrapper">
        <div id="reader-page">

          Header-
          {this.props.searchTerm}
          <br />
          <br />

          {searchedNewsOut}
        </div>
      </div>
    );
  }
}

Read.propTypes = {
  searchTerm: PropTypes.string,
};

Read.defaultProps = {
  searchTerm: '',
};

export default Read;
